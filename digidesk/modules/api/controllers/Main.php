<?php

namespace thebuggenie\modules\api\controllers;

use thebuggenie\core\framework,
    thebuggenie\core\entities,
    thebuggenie\core\entities\tables;

/**
 * Main actions for the api module
 *
 * @Routes(name_prefix="api_", url_prefix="/api/v1")
 */
class Main extends ApiController
{

    /**
     * Return generic information about this installation
     *
     * @Route(name="info", url="/info")
     *
     * @param framework\Request $request
     */
    public function runInform(framework\Request $request)
    {   
        $information = [
            'api_version' => $this->getApiVersion(),
            'version' => framework\Settings::getVersion(),
            'version_long' => framework\Settings::getVersion(true, true),
            'site_name' => framework\Settings::getSiteHeaderName(),
            'host' => framework\Settings::getURLhost(),
            'urls' => [
                'site' => (framework\Settings::getHeaderLink() == '') ? framework\Context::getWebroot() : framework\Settings::getHeaderLink(),
                'logo' => framework\Settings::getHeaderIconURL(),
                'icon' => framework\Settings::getFaviconURL()
            ],
            'online' => !framework\Settings::isMaintenanceModeEnabled()
        ];
        if (framework\Settings::hasMaintenanceMessage() && !$information['online']) {
            $information['maintenance_message'] = framework\Settings::getMaintenanceMessage();
        }

        $this->information = $information;
         return $this->renderJSON(array('info'=>$information));
        
    }
  public function runAuthenticate(framework\Request $request)
    {
        framework\Logging::log('Authenticating new application password.', 'api', framework\Logging::LEVEL_INFO);

        $username = trim($request['username']);
        $password = trim($request['password']);
        if ($username)
        {
            $user = tables\Users::getTable()->getByUsername($username);
		
            if ($password && $user instanceof entities\User)
            {
                //echo("The api app pwd ".$app_password->getHashPassword());
                foreach ($user->getApplicationPasswords() as $app_password)
                {
			//echo("The api app pwd ".$app_password->getHashPassword());
                    // Only return the token for new application passwords!
                    if (!$app_password->isUsed())
                    {
                        if (password_verify($password, $app_password->getHashPassword()))
                        {
                            $token = $app_password->verify();
                            $app_password->save();
                            return $this->renderJSON(array('token' => $token, 'name' => $app_password->getName(), 'created_at' => $app_password->getCreatedAt()));
                        }
                    }
                }
            }
            framework\Logging::log('No password matched.', 'api', framework\Logging::LEVEL_INFO);
        }

        $this->getResponse()->setHttpStatus(400);
        return $this->renderJSON(array('error' => "Incorrect username ({$username}) or application password ({$password})"));
    }
}
