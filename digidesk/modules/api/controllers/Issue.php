<?php

namespace thebuggenie\modules\api\controllers;

use thebuggenie\core\framework,
    thebuggenie\core\entities,
    thebuggenie\core\entities\tables,
    thebuggenie\core\modules\main\controllers;

/** @noinspection PhpInconsistentReturnPointsInspection */

/**
 * Workflow actions for the api module
 *
 * @property entities\Project[] $projects
 * @property entities\Issuetype[] $issuetypes
 * @property entities\Issue $issue
 * @property array $json
 *
 * @Routes(name_prefix="api_issue_", url_prefix="/api/v1/projects/:project_id/issues/:issue_no")
 */
class Issue extends ProjectNamespacedController
{

    /**
     * The currently selected project in actions where there is one
     *
     * @param framework\Request $request
     * @param $action
     */
    public function preExecute(framework\Request $request, $action)
    {
        parent::preExecute($request, $action);
        try
        {
            $issue = entities\Issue::getIssueFromLink($request['issue_no']);
        }
        catch (\Exception $e)
        {
            $this->getResponse()->setHttpStatus(500);
            return $this->renderJSON(array('error' => 'An exception occurred: ' . $e));
        }
         //Ranjini commented
//        if (!$issue instanceof entities\Issue || $issue->getProject()->getID() != $this->selected_project->getID())
  //      {
    //        $this->getResponse()->setHttpStatus(404);
      //      return $this->renderJSON(array('error' => 'Issue not found or not valid for this project'));
       // }

        $this->issue = $issue;
    }



   
    
    protected function _loadSelectedProjectAndIssueTypeFromRequestForReportIssueAction(framework\Request $request)
    {
        try
        {
            if ($project_key = $request['project_key'])
                $this->selected_project = entities\Project::getByKey($project_key);
            elseif ($project_id = $request['project_id'])
                $this->selected_project = entities\Project::getB2DBTable()->selectById($project_id);
        }
        catch (\Exception $e)
        {
              return $this->renderJSON(array("message"=>$e->getMessage()));
        }
        if ($this->selected_project instanceof entities\Project)
            framework\Context::setCurrentProject($this->selected_project);
        if ($this->selected_project instanceof entities\Project)
            $this->issuetypes = $this->selected_project->getIssuetypeScheme()->getIssuetypes();
        else
            $this->issuetypes = entities\Issuetype::getAll();

        $this->selected_issuetype = null;
        if ($request->hasParameter('issuetype'))
            $this->selected_issuetype = entities\Issuetype::getByKeyish($request['issuetype']);

        $this->locked_issuetype = (bool) $request['lock_issuetype'];

        if (!$this->selected_issuetype instanceof entities\Issuetype)
        {
            $this->issuetype_id = $request['issuetype_id'];
            if ($this->issuetype_id)
            {
                try
                {
                    $this->selected_issuetype = entities\Issuetype::getB2DBTable()->selectById($this->issuetype_id);
                }
                catch (\Exception $e)
                {

                }
            }


        }
        else
        {
            $this->issuetype_id = $this->selected_issuetype->getID();
        }
    }



    protected function _postIssueValidation(framework\Request $request, &$errors, &$permission_errors)
    {
        $i18n = framework\Context::getI18n();
        if (!$this->selected_project instanceof entities\Project)
            $errors['project'] = $i18n->__('You have to select a valid project');
        if (!$this->selected_issuetype instanceof entities\Issuetype)
            $errors['issuetype'] = $i18n->__('You have to select a valid issue type');
        if (empty($errors))
        {   
            $fields_array = $this->selected_project->getReportableFieldsArray($this->issuetype_id);
            $this->title = $request->getRawParameter('title',null);
            $this->selected_shortname = $request->getRawParameter('shortname', null);
            $this->selected_description = $request->getRawParameter('description',null);
            if (trim($this->title) == '' || $this->title == $this->default_title)
                $errors['title'] = true;
 
           

           if (isset($fields_array['shortname']) && $fields_array['shortname']['required'] && trim($this->selected_shortname) == '')
                $errors['shortname'] = true;
            if (isset($fields_array['description']) && $fields_array['description']['required'] && trim($this->selected_description) == '')
                $errors['description'] = true;

            if ($status_id = (int) $request['status_id'])
                $this->selected_status = entities\Status::getB2DBTable()->selectById($status_id);

            if ($parent_issue_id = (int) $request['parent_issue_id'])
                $this->parent_issue = entities\Issue::getB2DBTable()->selectById($parent_issue_id);

            if ($resolution_id = (int) $request['resolution_id'])
                $this->selected_resolution = entities\Resolution::getB2DBTable()->selectById($resolution_id);

            if ($severity_id = (int) $request['severity_id'])
                $this->selected_severity = entities\Severity::getB2DBTable()->selectById($severity_id);

            if ($priority_id = (int) $request['priority_id'])
                $this->selected_priority = entities\Priority::getB2DBTable()->selectById($priority_id);

            $selected_customdatatype = array();
            
            foreach (entities\CustomDatatype::getAll() as $customdatatype)
            {
                $customdatatype_id = $customdatatype->getKey() . '_id';
                   
                $customdatatype_value = $customdatatype->getKey() . '_value';

                $flag=false; 
                if ($customdatatype->hasCustomOptions())
                {
                    foreach($customdatatype->getOptions() as $option)
                        if($option->getValue() == $request->getParameter($customdatatype_id))
                         {     $customdata_id = $option->getID(); break; }
                      
                    $selected_customdatatype[$customdatatype->getKey()] = null;
                    try{
                    if ($request->hasParameter($customdatatype_id) && isset($customdata_id))
                    {
                        $selected_customdatatype[$customdatatype->getKey()] = new entities\CustomDatatypeOption((int)$customdata_id);
                    }
                  }
                     catch (\Exception $e)
                        {  return $this->renderJSON(array("fail"=>$e->getMessage())); }
               
                }

                else
                {
                    $selected_customdatatype[$customdatatype->getKey()] = null;
                    switch ($customdatatype->getType())
                    {
                        case entities\CustomDatatype::INPUT_TEXTAREA_MAIN:
                        case entities\CustomDatatype::INPUT_TEXTAREA_SMALL:
                            if ($request->hasParameter($customdatatype_value))
                                $selected_customdatatype[$customdatatype->getKey()] = $request->getParameter($customdatatype_value, null, false);

                            break;
                        default:
                            if ($request->hasParameter($customdatatype_value)){
                                $selected_customdatatype[$customdatatype->getKey()] = $request->getParameter($customdatatype_value);
						 
}
                            elseif ($request->hasParameter($customdatatype_id))
                                $selected_customdatatype[$customdatatype->getKey()] = $request->getParameter($customdatatype_id);

                            break;
                    }
                }
            }
            $this->selected_customdatatype = $selected_customdatatype;

            foreach ($fields_array as $field => $info)
            {
                if ($field == 'user_pain')
                {
                    if ($info['required'])
                    {
                        if (!($this->selected_pain_bug_type != 0 && $this->selected_pain_likelihood != 0 && $this->selected_pain_effect != 0))
                        {
                            $errors['user_pain'] = true;
                        }
                    }
                }
                elseif ($info['required'])
                {
                    $var_name = "selected_{$field}";
                    if ((in_array($field, entities\Datatype::getAvailableFields(true)) && ($this->$var_name === null || $this->$var_name === 0)) || (!in_array($field, entities\DatatypeBase::getAvailableFields(true)) && !in_array($field, array('pain_bug_type', 'pain_likelihood', 'pain_effect')) && (array_key_exists($field, $selected_customdatatype) && $selected_customdatatype[$field] === null)))
                    {
                        $errors[$field] = true;
                    }
                }
                else
                {
                    if (in_array($field, entities\Datatype::getAvailableFields(true)) || in_array($field, array('pain_bug_type', 'pain_likelihood', 'pain_effect')))
                    {
                        if (!$this->selected_project->fieldPermissionCheck($field))
                        {
                            $permission_errors[$field] = true;
                        }
                    }
                    elseif (!$this->selected_project->fieldPermissionCheck($field, true, true))
                    {
                        $permission_errors[$field] = true;
                    }
                }
            }
            $event = new \thebuggenie\core\framework\Event('core', 'mainActions::_postIssueValidation', null, array(), $errors);
            $event->trigger();
            $errors = $event->getReturnList();
        }
       //return !(bool) (count($errors) + count($permission_errors));
        return $errors;
    }




    protected function _postIssue(framework\Request $request)
    { 
        $fields_array = $this->selected_project->getReportableFieldsArray($this->issuetype_id);
        $issue = new entities\Issue(); 
        $issue->setTitle($this->title);
        $issue->setIssuetype($this->issuetype_id);
        $issue->setProject($this->selected_project);
        if (isset($fields_array['shortname']))
            $issue->setShortname($this->selected_shortname);
        if (isset($fields_array['description'])) {
            $issue->setDescription($this->selected_description);
            $issue->setDescriptionSyntax($this->selected_description_syntax);
        }
        if (isset($fields_array['status']) && $this->selected_status instanceof entities\Datatype)
            $issue->setStatus($this->selected_status->getID());
        if (isset($fields_array['resolution']) && $this->selected_resolution instanceof entities\Datatype)
            $issue->setResolution($this->selected_resolution->getID());
        if (isset($fields_array['severity']) && $this->selected_severity instanceof entities\Datatype)
            $issue->setSeverity($this->selected_severity->getID());
        if (isset($fields_array['priority']) && $this->selected_priority instanceof entities\Datatype)
            $issue->setPriority($this->selected_priority->getID());
        foreach (entities\CustomDatatype::getAll() as $customdatatype)
        {
            if (!isset($fields_array[$customdatatype->getKey()]))
                continue;
            if ($customdatatype->hasCustomOptions())
            {
                if (isset($fields_array[$customdatatype->getKey()]) && $this->selected_customdatatype[$customdatatype->getKey()] instanceof entities\CustomDatatypeOption)
                {
                    $selected_option = $this->selected_customdatatype[$customdatatype->getKey()];
                    $issue->setCustomField($customdatatype->getKey(), $selected_option->getID());
                }
            }
            else
            {
                $issue->setCustomField($customdatatype->getKey(), $this->selected_customdatatype[$customdatatype->getKey()]);
            }
        }

        // FIXME: If we set the issue assignee during report issue, this needs to be set INSTEAD of this
        if ($this->selected_project->canAutoassign())
        {
            if (isset($fields_array['component']) && $this->selected_component instanceof entities\Component && $this->selected_component->hasLeader())
            {
                $issue->setAssignee($this->selected_component->getLeader());
            }
            elseif (isset($fields_array['edition']) && $this->selected_edition instanceof entities\Edition && $this->selected_edition->hasLeader())
            {
                $issue->setAssignee($this->selected_edition->getLeader());
            }
            elseif ($this->selected_project->hasLeader())
            {
                $issue->setAssignee($this->selected_project->getLeader());
            }
        }

        if ($request->hasParameter('custom_issue_access') && $this->selected_project->permissionCheck('canlockandeditlockedissues'))
        {
            switch ($request->getParameter('issue_access'))
            {
                case 'public':
                case 'public_category':
                    $issue->setLocked(false);
                    $issue->setLockedCategory($request->hasParameter('public_category'));
                    break;
                case 'restricted':
                    $issue->setLocked();
                    break;
            }
        }
        else
        {
            $issue->setLockedFromProject($this->selected_project);
        }

       // framework\Event::listen('core', 'thebuggenie\core\entities\Issue::createNew_pre_notifications', array($this, 'listen_issueCreate'));
        $issue->save();

        if (isset($this->parent_issue))
            $issue->addParentIssue($this->parent_issue);
        return $issue;
    }


    protected function _getParentIssueFromRequest($request)
    {
        if ($request->hasParameter('parent_issue_id'))
        {
            try
            {
                $parent_issue = entities\Issue::getB2DBTable()->selectById((int) $request['parent_issue_id']);
                return $parent_issue;
            }
            catch (\Exception $e) { }
        }
    }




    //Raising an issue
     public function runReportIssue(framework\Request $request) {


        $date = date("d-m-Y h:m:s");
        $req_log = "\n \n CREATE IGMS: [$date] ".json_encode($request->getParameters())."";
        error_log($req_log,3,'/opt/lampp/logs/access.log');
        $i18n = framework\Context::getI18n();
        
        $errors = array();
        $permission_errors = array();
        $this->issue = null;
       $this->_loadSelectedProjectAndIssueTypeFromRequestForReportIssueAction($request);
        $this->forward403unless(framework\Context::getCurrentProject() instanceof entities\Project && framework\Context::getCurrentProject()->hasAccess() && $this->getUser()->canReportIssues(framework\Context::getCurrentProject()));

        if ($request->isPost())
        {
            if ($this->_postIssueValidation($request, $errors, $permission_errors))
            {
                try{
                    $issue = $this->_postIssue($request);
                    $uniqueid = uniqid('',true);
                    $issue->setCustomField('entityreferencenumber',$uniqueid);
                    $issue->save();
                    if ($request->hasParameter('files') && $request->hasParameter('file_description'))
                    {
                        $files = $request['files'];
                        $file_descriptions = $request['file_description'];
                        foreach ($files as $file_id => $nothing)
                        {
                            $file = entities\File::getB2DBTable()->selectById((int) $file_id);
                            $file->setDescription($file_descriptions[$file_id]);
                            $file->save();
                            tables\IssueFiles::getTable()->addByIssueIDandFileID($issue->getID(), $file->getID());
                        }
                    }
                   if ($request['return_format'] == 'planning')
                    {   
                        $this->_loadSelectedProjectAndIssueTypeFromRequestForReportIssueAction($request);
                        $options = array();
                        $options['selected_issuetype'] = $issue->getIssueType();
                        $options['selected_project'] = $this->selected_project;
                        $options['issuetypes'] = $this->issuetypes;
                        $options['issue'] = $issue;
                        $options['errors'] = $errors;
                        $options['permission_errors'] = $permission_errors;
                        $options['selected_milestone'] = $this->_getMilestoneFromRequest($request);
                        $options['selected_build'] = $this->_getBuildFromRequest($request);
                        $options['parent_issue'] = $this->_getParentIssueFromRequest($request);
                        $options['medium_backdrop'] = 1;
                        return $this->renderJSON(array('content' => $this->getComponentHTML('main/reportissuecontainer', $options)));
                    } 
                    
                        $this->issue = $issue;
                        return $this->renderJSON(array("issueno"=>$issue->getIssueNo(),"entityreferencenumber"=>$issue->getFieldValue('entityreferencenumber')));
 
                }
                catch (\Exception $e)
                {
                    if ($request['return_format'] == 'planning')
                    {
                        $this->getResponse()->setHttpStatus(400);
                        return $this->renderJSON(array('error' => $e->getMessage()));
                    }
                    $errors[] = $e->getMessage();
                }
            }
        }
        if ($request['return_format'] == 'planning')
        {
            $err_msg = array();
            foreach ($errors as $field => $value)
            {
                $err_msg[] = $i18n->__('Please provide a value for the %field_name field', array('%field_name' => $field));
            }
            foreach ($permission_errors as $field => $value)
            {
                $err_msg[] = $i18n->__("The %field_name field is marked as required, but you don't have permission to set it", array('%field_name' => $field));
            }
            $this->getResponse()->setHttpStatus(400);
            return $this->renderJSON(array('error' => $i18n->__('An error occured while creating this story: %errors', array('%errors' => '')), 'message' => join('<br>', $err_msg)));
        }
        $this->errors = $errors;
        $this->permission_errors = $permission_errors;
        $this->options = $this->getParameterHolder();



   }



    /**
     * @Route(name="get", url="/", methods="GET")
     * @param framework\Request $request
     * @throws \Exception
     */
    public function runViewIssue(framework\Request $request)
    {
    }


    /**
     * @Route(name="update", url="/", methods="POST")
     * @param framework\Request $request
     : @throws \Exception
     */


    public function runUpdateIssueDetails(framework\Request $request)
    
  {  
        $date = date('d-m-Y h:m:s');
        $req_log = "\n \n IGMS UPDATE: [$date] ".json_encode($request->getParameters())."";
      error_log($req_log,3,'/opt/lampp/logs/access.log');
   
         
           try
          {
              if ($project_key = $request->getRawParameter('project_key'))
                  $this->selected_project = entities\Project::getByKey($project_key);
              elseif ($project_id = $request->getRawParameter('project_id'))
                  $this->selected_project = entities\Project::getB2DBTable()->selectById($project_id);
          }
          catch (\Exception $e)
          {
                 return $this->renderJSON(array("Fail"=>"Invalid project"));
          }
  
          
         if (($issue_id = $request->getRawParameter('issue_id')) && ($project_id = $request->getRawParameter('project_id')))
          {
  
              try
              {
                  $issue = tables\Issues::getTable()->getByProjectIDAndIssueNo($project_id,$issue_id);
                  $this->issue = $issue;
               if(!$issue)
                    return $this->renderJSON(array("Fail"=>"Failed to get the issue"));

              }
              catch (\Exception $e)
              {
                  $this->getResponse()->setHttpStatus(400);
                   return $this->renderJSON(array("fail"=>"Failed to get issue"));
              }
          }
          else
          {
              $this->getResponse()->setHttpStatus(400);
               return $this->renderJSON(array("fail"=>"no issue found"));
          }
    
     
          
          $this->forward403if($this->selected_project->isArchived());
          $this->error = false;
          try
          {
              $i18n = framework\Context::getI18n();
  
              $workflow_transition = null;
              if ($passed_transition = $request->getRawParameter('workflow_transition'))
              {
                  //echo "looking for transition ";
                  $key = str_replace(' ', '', mb_strtolower($passed_transition));
                  //echo $key . "\n";
                  foreach ($this->issue->getAvailableWorkflowTransitions() as $transition)
                  {
                      //echo str_replace(' ', '', mb_strtolower($transition->getName())) . "?";
                      if (mb_strpos(str_replace(' ', '', mb_strtolower($transition->getName())), $key) !== false)
                      {
                          $workflow_transition = $transition;
                          //echo "found transition " . $transition->getID();
                          break;
                      }
                      //echo "no";
                  }
  
                  if (!$workflow_transition instanceof entities\WorkflowTransition)
                      throw new \Exception("This transition ({$key}) is not valid");
              }
              $fields = $request->getRawParameter('fields', array());
              $return_values = array();
              if ($workflow_transition instanceof entities\WorkflowTransition)
              {
                  foreach ($fields as $field_key => $field_value)
                  {
                      $classname = "\\thebuggenie\\core\\entities\\" . ucfirst($field_key);
                      $method = "set" . ucfirst($field_key);
                      $choices = $classname::getAll();
                      $found = false;
                      foreach ($choices as $choice_key => $choice)
                      {
                          if (mb_strpos(str_replace(' ', '', mb_strtolower($choice->getName())), str_replace(' ', '', mb_strtolower($field_value))) !== false)
                          {
                              $request->setParameter($field_key . '_id', $choice->getId());
                              break;
                          }
                      }
                  }
                  $request->setParameter('comment_body', $request->getRawParameter('message'));
                  $return_values['applied_transition'] = $workflow_transition->getName();
                  if ($workflow_transition->validateFromRequest($request))
                  {
                      $retval = $workflow_transition->transitionIssueToOutgoingStepFromRequest($this->issue, $request);
                      $return_values['transition_ok'] = ($retval === false) ? false : true;
                  }
                  else
                  {
                      $return_values['transition_ok'] = false;
                      $return_values['message'] = "Please pass all information required for this transition";
                  }
              }
              if($this->issue->isUpdateable())
              {   
              foreach ($fields as $field_key => $field_value)
                  { 
                      try
                        {
                          if (in_array($field_key, array_merge(array('title', 'state'), entities\Datatype::getAvailableFields())))
                          {
                              switch ($field_key)
                              {
                                  case 'state':
                                      $this->issue->setState(($field_value == 'open') ? entities\Issue::STATE_OPEN : entities\Issue::STATE_CLOSED);
                                      break;
                                  case 'title':
                                      if ($field_value != '')
                                          $this->issue->setTitle($field_value);
                                      else
                                          throw new \Exception($i18n->__('Invalid title'));
                                      break;
                                  case 'shortname':
                                  case 'description':
                                  case 'reproduction_steps':
                                      $method = "set" . ucfirst($field_key);
                                      $this->issue->$method($field_value);
                                      break;
                                  case 'status':
                                  case 'resolution':
                                  case 'reproducability':
                                  case 'priority':
                                  case 'severity':
                                  case 'category':
                                      $classname = "\\thebuggenie\\core\\entities\\" . ucfirst($field_key);
                                      $method = "set" . ucfirst($field_key);
                                      $choices = $classname::getAll();
                                      $found = false;
                                      foreach ($choices as $choice_key => $choice)
                                      {
                                          if (str_replace(' ', '', mb_strtolower($choice->getName())) == str_replace(' ', '', mb_strtolower($field_value)))
                                          {
                                              $this->issue->$method($choice);
                                              $found = true;
                                          }
                                      }
                                      if (!$found)
                                      {
                                          throw new \Exception('Could not find this value');
                                      }
                                      break;
                                  case 'percent_complete':
                                      $this->issue->setPercentCompleted($field_value);
                                      break;
                                  case 'owner':
                                  case 'assignee':
                                      $set_method = "set" . ucfirst($field_key);
                                      $unset_method = "un{$set_method}";
                                      switch (mb_strtolower($field_value))
                                      {
                                          case 'me':
                                              $this->issue->$set_method(framework\Context::getUser());
                                              break;
                                          case 'none':
                                              $this->issue->$unset_method();
                                              break;
                                          default:
                                              try
                                              {
                                                  $user = entities\User::findUser(mb_strtolower($field_value));
                                                  if ($user instanceof entities\User)
                                                      $this->issue->$set_method($user);
                                              }
                                              catch (\Exception $e)
                                              {
                                                  throw new \Exception('No such user found');
                                              }
                                              break;
                                      }
                                      break;
                                  case 'estimated_time':
                                  case 'spent_time':
                                      $set_method = "set" . ucfirst(str_replace('_', '', $field_key));
                                      $this->issue->$set_method($field_value);
                                      break;
                                  case 'milestone':
                                      $found = false;
                                      foreach ($this->selected_project->getMilestones() as $milestone)
                                      {
                                          if (str_replace(' ', '', mb_strtolower($milestone->getName())) == str_replace(' ', '', mb_strtolower($field_value)))
                                          {
                                              $this->issue->setMilestone($milestone->getID());
                                              $found = true;
                                          }
                                      }
                                      if (!$found)
                                      {
                                          throw new \Exception('Could not find this milestone');
                                      }
                                      break;
                                  default:
                                  //    throw new \Exception($i18n->__('Invalid field'));
                              }
                          
                          if ($customdatatype = entities\CustomDatatype::getByKey($field_key))
                  {
                      $key = $customdatatype->getKey();
                      $customdatatypeoption_value = $field_value;
                      if (true)
                      {

                          switch ($customdatatype->getType())
                          {  
                              case entities\CustomDatatype::INPUT_TEXTAREA_MAIN:
                              case entities\CustomDatatype::INPUT_TEXTAREA_SMALL:
                               case entities\CustomDatatype::INPUT_TEXT:
                       try{
                            if ($customdatatypeoption_value == '')
                                  {
                                      $issue->setCustomField($key, "");
                                  }
                                  else
                                  {
                                      $issue->setCustomField($key, $field_value);
                                  }
                                  $issue->save();
                                  $changed_methodname = "isCustomfield{$key}Changed";
                      }
                                  catch (\Exception $e) {

                              return  $this->renderJSON(array('fail'=>"failed updating ".$field_key));

  }

                                  array_push($return_values,($customdatatypeoption_value == '') ? array('changed' => true, 'field' => array('id' => 0)) : array('changed' => true, 'name' => $key, 'value' => tbg_parse_text($field_value)));break;
                              case entities\CustomDatatype::DATE_PICKER:
                              case entities\CustomDatatype::DATETIME_PICKER:
                             try{
                                  if ($customdatatypeoption_value == '')
                                  {
                                      $issue->setCustomField($key, "");
                                  }
                                  else
                                  {
                                      $issue->setCustomField($key, $field_value);
                                  }
                                  $issue->save();
                                  $changed_methodname = "isCustomfield{$key}Changed";
                               }
                             catch (\Excception $e) {
                                      return $this->renderJSON(array('fail'=>"failed updating ".$field_key));
  }
                                  array_push($return_values,($customdatatypeoption_value == '') ? array('changed' => true, 'field' => array('id' => 0)) : array('changed' => true,'name' => $key, 'value' => date('Y-m-d' . ($customdatatype->getType() == entities\CustomDatatype::DATETIME_PICKER ? ' H:i' : ''), (int) $field_value)));break;
                               default:
                               $flag=false;
                               try{
                                  
                                  if ($customdatatypeoption_value == '')
                                  {
                                      $issue->setCustomField($key, "");
                                  }
                                  else
                                  {   $options = $customdatatype->getOptions();
                                      foreach($options as $option=>$prop)
                                             if($prop->getValue() == $field_value) {                           
                                                $issue->setCustomField($key, $option);
                                                $flag=true;
                                           }
                                  }
                                  
                                  $issue->save();
                                  $changed_methodname = "isCustomfield{$key}Changed";
                              }
                              catch (\Exception $e) {
                                      return $this->renderJSON(array("fail"=>'failed  updating '.$field_key));
  }
        if(!$flag)
                      array_push($return_values,array("fail"=>"failed updating ".$field_key));                              
     else
    array_push($return_values,($customdatatypeoption_value == '') ? array('changed' => true, 'field' => array('id' => 0)) : array('changed' => true,'name' => $key, 'value' => $customdatatypeoption_value));

                  }
  
  
  
  
                          }
                         // $return_values[$field_key] = array('success' => true);
                      }
                  }
					  }
                      catch (\Exception $e)
                      {
                          $return_values[$field_key] = array('success' => false, 'error' => $e->getMessage());
                      }
                  
              }
  
              if (!$workflow_transition instanceof entities\WorkflowTransition)
                  $this->issue->getWorkflow()->moveIssueToMatchingWorkflowStep($this->issue);
  
              if (!array_key_exists('transition_ok', $return_values) || $return_values['transition_ok'])
              {
                  $comment = new entities\Comment();
                  $comment->setContent($request->getParameter('message', null, false));
                  $comment->setPostedBy(framework\Context::getUser()->getID());
                  $comment->setTargetID($this->issue->getID());
                  $comment->setTargetType(entities\Comment::TYPE_ISSUE);
                  $comment->setModuleName('core');
                  $comment->setIsPublic(true);
                  $comment->setSystemComment(false);
                  $comment->save();
                  $this->issue->setSaveComment($comment);
                  $this->issue->save();
              }
           // $final_array = array("response"=>"");
           // $return_values = array("response"=>json_encode($return_values));
           // $final_array["response"] = json_encode($return_values);
             $this->return_values = $return_values;
            // $final_array = array();
            // array_push($final_array,$return_values);
            $response = str_replace("[","{",json_encode($return_values));
            $response = str_replace("]","}",$response);
             return $this->renderJSON($response);
          }
		  }
          catch (\Exception $e)
          {
              //$this->getResponse()->setHttpStatus(400);
              return $this->renderJSON(array('fail'=>"failed with message ".$e->getMessage()));
           }
    }






    /**
     * @Route(name="list_transitions", url="/transitions")
     * @param framework\Request $request
     * @throws \Exception
     */
    public function runListTransitions(framework\Request $request)
    {
        $transitions = [];
        foreach ($this->issue->getAvailableWorkflowTransitions() as $transition)
        {
            $transitions[] = $transition->toJSON();
        }

        $this->transitions = $transitions;
    }

}
